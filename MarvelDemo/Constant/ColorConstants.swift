//
//  ColorConstants.swift
//  MarvelDemo
//
//  Created by iMac on 06/09/21.
//

import Foundation
import UIKit

/** This colors struct is used to define colors for application. */

struct ColorConstants {

    static let Theme                        = #colorLiteral(red: 0.3137254902, green: 0.05882352941, blue: 0.5490196078, alpha: 1)
   
    static let Orange                       = #colorLiteral(red: 0.9921568627, green: 0.4156862745, blue: 0.2274509804, alpha: 1)
    static let Border                       = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
    static let backGroundGray               = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
    static let backGroundTheme              = #colorLiteral(red: 0.4509803922, green: 0.2470588235, blue: 0.6392156863, alpha: 1)
    static let borderUserDetail             = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
    static let themeBorder                  = #colorLiteral(red: 0.4509803922, green: 0.2470588235, blue: 0.6392156863, alpha: 1)
    static let blueBorder                   = #colorLiteral(red: 0.1333333333, green: 0.8078431373, blue: 0.8470588235, alpha: 1)
    static let backGroundBGray              = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
    static let scoreConflict                = #colorLiteral(red: 1, green: 0.7137254902, blue: 0.7568627451, alpha: 1)
}

