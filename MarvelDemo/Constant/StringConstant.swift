//
//  StringConstant.swift
//  MarvelDemo
//
//  Created by iMac on 06/09/21.
//

import UIKit

struct StringConstant {
    
    static let NoInternetConnection         = "No Internet Connection."
    
    //NoDataMessage
    struct NoDataMessage {
        
        static let noDataFound              = "No data found."
    }
}
